const entries = ["src/player.ts", "src/bootstrap.ts", "src/runtime.ts"];

// if (process.env.NODE_ENV === "development") {
//   entries.push("src/editor.ts");
// }

const buildSettings = {
  entryPoints: entries,
  bundle: true,
  minify: process.env.NODE_ENV !== "development",
  minifyWhitespace: process.env.NODE_ENV !== "development",
  sourcemap: process.env.NODE_ENV === "development",
  outdir: `dist`,
  plugins: [],
  loader: {},
  define: {
    "process.env.NODE_ENV": `"${process.env.NODE_ENV}"`,
  },
  watch: process.env.NODE_ENV === "development",
};

require("esbuild")
  .build(buildSettings)
  .catch(() => process.exit(1));
