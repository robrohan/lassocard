// This file is embedded into the SVG to setup the 
// document in a consistent way. The designer will have to
// add this script to the SVG document (like with inkscape 
// for example, but shouldn't need mess with aside from that.
//
// The tag *requires* an id of "lc_bootstrap", and a data-meta
// attribute that points to the scene file:
//
//   <script
//      type="text/javascript"
//      href="../../bootstrap.js"
//      data-meta="scene.json"
//      id="lc_bootstrap" />
//

export type Scene = {
  name: string;
}

export type MetaData = {
  root: string,
  canvas: [number, number],
  graphics: string,
  scripts: Array<string>,
  style: string,
  scenes: Array<Scene>,
}

const docRoot = document.documentElement;

const init = (meta: MetaData) => {
  // Install a camera that we can control around all the "scenes"
  // we are assuming each scene is it's own layer
  const gs = docRoot.querySelectorAll("svg > g");
  const camera = document.createElementNS("http://www.w3.org/2000/svg", "g");
  camera.setAttribute("id", "camera");
  camera.setAttribute("transform", "matrix(1, 0, 0, 1, 0, 0)");
  Array.from(gs).forEach((d) => {
    console.log(d.getAttribute("id"));
    camera.appendChild(d);
  });
  docRoot.appendChild(camera);

  // Install a custom stylesheet
  const style = document.createElementNS(
    "http://www.w3.org/1999/xhtml",
    "link"
  );
  style.setAttribute("rel", "stylesheet");
  style.setAttribute("href", meta.style);
  style.setAttribute("type", "text/css");
  docRoot.appendChild(style);

  // Install custom script for the experience
  meta.scripts.forEach(uri => {
    const script = document.createElementNS(
      "http://www.w3.org/1999/xhtml",
      "script"
    );
    script.setAttribute("type", "module");
    script.setAttribute("id", `g${Math.floor(Math.random() * 100000)}`);
    script.setAttribute("src", uri);
    docRoot.appendChild(script);
    // script.addEventListener('load', function () {
    //   // console.log("loaded!");
    // });
  })
}

const metaFile = docRoot.querySelector("#lc_state");
if (metaFile === undefined) {
  alert("Metadata is missing.")
}
const meta = JSON.parse(metaFile.innerHTML);
init(meta);

// const metaFile = docRoot
//   .querySelector("#lc_bootstrap")
//   .getAttribute("data-meta");
// fetch(metaFile)
//   .then((r) => r.json())
//   .then((j) => {
//     init(j);
//   });
