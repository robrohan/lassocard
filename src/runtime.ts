// This file can be loaded by the client provided app
// They have to explicitly import it, but it is loaded
// at runtime by the browser - not compile time.

const SODIPODI_NS = "http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd";

export type Symbol = {
  type: string,
  id: string,
  w: number,
  h: number,
  x: number,
  y: number
}

(window as any)['getDoc'] = (): Document => {
  return (document.documentElement as any) as Document;
};

(window as any)['getSymbol'] = (id: string) => {
  return (window as any).getDoc().getElementById(id);
};

(window as any)['targetInfo'] = (target: HTMLElement) => {
  const type = target.localName;
  const id = target.getAttribute("id");
  const bbox = (target as any).getBBox();
  const w = bbox.width;
  const h = bbox.height;
  const x =
    parseInt(target.getAttribute("x")) ||
    parseInt(target.getAttribute("cx")) ||
    parseInt(target.getAttributeNS(SODIPODI_NS, "cx")) ||
    NaN;
  const y =
    parseInt(target.getAttribute("y")) ||
    parseInt(target.getAttribute("cy")) ||
    parseInt(target.getAttributeNS(SODIPODI_NS, "cy")) ||
    NaN;
  return { type, id, w, h, x, y };
};

(window as any)['targetInfoFromEvent'] = (evt: MouseEvent) => {
  const target = evt.target;
  return (window as any).targetInfo(target as HTMLElement);
};

