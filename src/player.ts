// This file will be used by the web application to
// include the player in the first place. This will be
// all an "end user" of the library would need. 
//
// <script src="player.js" scene="hive/ink/scene.json"></script>
//
// See index.html
//
const sceneUrl = document.currentScript.getAttribute('scene');
if (!sceneUrl) {
  throw Error("Missing scene descriptor");
}

fetch(sceneUrl).then(r => r.json()).then(scene => {
  const experience = `${scene.root}/${scene.graphics}`;

  const obj = document.createElement("object");
  obj.setAttribute("type", "image/svg+xml");
  obj.setAttribute("data", experience);
  obj.setAttribute("width", scene.canvas[0] + '');
  obj.setAttribute("height", scene.canvas[1] + '');

  const fallback = document.createElement("img");
  fallback.setAttribute("src", experience);
  fallback.setAttribute("width", scene.canvas[0] + '');
  fallback.setAttribute("height", scene.canvas[1] + '');
  obj.append(fallback);

  document.body.append(obj);
}).catch(err => {
  throw Error(`Player error: ${err.message}`);
});

