// import * as r from "/runtime.js";
// const module = await import("/runtime.js");

const scenes = document.querySelectorAll("#camera > g");
const scenesArray = Array.from(scenes).reverse();
let currentScene = 1;
const goToScene = (idx) => {
  const sr = scenesArray;
  console.log(sr.length, idx);
  for (let i = 0; i < sr.length; i++) {
    const s = sr[i];
    if (i === idx) {
      s.style.display = "block";
    } else {
      s.style.display = "none";
    }
  }
};

const goToNextScene = () => {
  if (currentScene >= scenesArray.length) {
    currentScene = 0;
  }
  goToScene(currentScene++);
};

const goToPreviousScene = () => {
  if (currentScene < 0) {
    currentScene = scenesArray.length;
  }
  goToScene(--currentScene);
};

///////////////////////////////////////////////////////
const moveSpeed = 5;

const cv = [0, 0];
const tv = [0, 0];

const player = getSymbol("player");

// Start offset position
const sym = targetInfo(player);
cv[0] = sym.x + sym.w / 2 || 0;
cv[1] = sym.y + sym.h / 2 || 0;

player.addEventListener("click", (e) => {
  const say = e.target.getAttribute("data-click");
  console.log(say);
  // const audio = document.getElementById("trackHTML5");
  // audio.play();
});

getDoc().addEventListener("mousedown", (e) => {
  const symbol = targetInfoFromEvent(e);
  console.log(e, symbol);

  tv[0] = e.clientX - cv[0];
  tv[1] = e.clientY - cv[1];

  player.style.transform = `translate(${tv[0]}px, ${tv[1]}px)`;
  player.style.transition = `transform .3s ease-out`;

  if (e.buttons === 4) goToNextScene();
  // else if (e.buttons === 4) goToPreviousScene();
});

// getDoc().addEventListener(
//   "keydown",
//   function (evt) {
//     let [x, y] = tv;
//     switch (evt.key) {
//       case "w":
//         player.style.transform = `translate(${x}px, ${(y -= moveSpeed)}px)`;
//         break;
//       case "s":
//         player.style.transform = `translate(${x}px, ${(y += moveSpeed)}px)`;
//         break;
//       case "a":
//         player.style.transform = `translate(${(x -= moveSpeed)}px, ${y}px)`;
//         break;
//       case "d":
//         player.style.transform = `translate(${(x += moveSpeed)}px, ${y}px)`;
//         break;
//     }
//     tv[0] = x;
//     tv[1] = y;
//   },
//   false
// );

// main game loop
// const loop = (t) => {
//   circle.style.transform = `translate(${tv[0]}px, ${tv[1]}px)`;
//   circle.style.transition = `transform .3s ease-out`;
//   requestAnimationFrame(loop);
// };
// requestAnimationFrame(loop);
