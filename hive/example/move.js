const moveSpeed = 5;
const docRoot = document.documentElement;

const cv = [0, 0];
const tv = [0, 0];

const circle = docRoot.getElementById("cic");
// let x = 0; // parseInt(circle.getAttribute("x")),
// y = 0; //parseInt(circle.getAttribute("y"));

circle.addEventListener("click", (e) => {
  const say = e.target.getAttribute("data-click");
  console.log(say);
  // const audio = document.getElementById("trackHTML5");
  // audio.play();
});

docRoot.addEventListener("mousedown", (e) => {
  console.log(e);

  tv[0] = e.clientX;
  tv[1] = e.clientY;
});

docRoot.addEventListener(
  "keydown",
  function (evt) {
    let [x, y] = tv;
    switch (evt.key) {
      case "w": // KEY.w:
        // circle.setAttribute("cy", (y -= moveSpeed));
        circle.style.transform = `translate(${x}px, ${(y -= moveSpeed)}px)`;
        // Alternatively:
        // circle.cy.baseVal.value = (y-=moveSpeed);
        break;
      case "s": //KEY.s:
        // circle.setAttribute("cy", (y += moveSpeed));
        circle.style.transform = `translate(${x}px, ${(y += moveSpeed)}px)`;
        break;
      case "a": //KEY.a:
        // circle.setAttribute("cx", (x -= moveSpeed));
        circle.style.transform = `translate(${(x -= moveSpeed)}px, ${y}px)`;
        break;
      case "d": //KEY.d:
        // circle.setAttribute("cx", (x += moveSpeed));
        circle.style.transform = `translate(${(x += moveSpeed)}px, ${y}px)`;
        break;
    }
    tv[0] = x;
    tv[1] = y;
  },
  false
);

const newLine = document.createElementNS("http://www.w3.org/2000/svg", "line");
newLine.setAttribute("id", "line2");
newLine.setAttribute("x1", "0");
newLine.setAttribute("y1", "0");
newLine.setAttribute("x2", "200");
newLine.setAttribute("y2", "200");
newLine.setAttribute("stroke", "black");

docRoot.getElementById("camera").append(newLine);

const loop = (t) => {
  circle.style.transform = `translate(${tv[0]}px, ${tv[1]}px)`;
  circle.style.transition = `transform .3s ease-out`;
  requestAnimationFrame(loop);
};
requestAnimationFrame(loop);
