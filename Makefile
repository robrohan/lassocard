.PHONY: all test clean

hash = $(shell git log --pretty=format:'%h' -n 1)

clean: 
	rm -rf dist

start:
	mkdir -p dist
	cp index.html dist/index.html
	cp -R hive dist
	NODE_ENV=development yarn build

build:
	yarn build
